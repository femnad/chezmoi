#!/usr/bin/env python3
import argparse
from dataclasses import dataclass
import shlex
import subprocess
import sys

DEFAULT_SINK = '@DEFAULT_AUDIO_SINK@'
DEFAULT_VOLUME_STEP = 5
MUTED_SUFFIX = '[MUTED]'
VOLUME_ICON = 'audio-volume-{icon}-symbolic'
VOLUME_LIMIT = 1.5


@dataclass
class Args:
    step: int = 0


@dataclass
class Volume:
    level: float
    muted: bool


def sh_base(*cmd: str):
    subprocess.run(cmd, check=True)


def notify_cmd_error(cmd: str, proc: subprocess.CompletedProcess):
    output = []
    stdout = proc.stdout.strip()
    stderr = proc.stderr.strip()

    if stdout:
        output.append(f'stdout: {stdout}')
    if stderr:
        output.append(f'stderr: {stderr}')

    joined = '\n'.join(output)

    sh_base('notify-send', cmd, joined)


def sh(cmdstr: str):
    cmd = shlex.split(cmdstr)
    proc = subprocess.run(cmd, capture_output=True, text=True)

    if proc.returncode:
        notify_cmd_error(cmdstr, proc)
        print(f'Error running `{cmdstr}`, output: {proc.stderr.strip()}')
        sys.exit(proc.returncode)

    return proc.stdout.strip()


class Wpctl:

    def change_volume(self, step: int, sign: str):
        step_f = step / 100
        sh(f'wpctl set-volume {DEFAULT_SINK} {step_f}{sign} -l {VOLUME_LIMIT}')

    def get_volume(self) -> Volume:
        vol = sh(f'wpctl get-volume {DEFAULT_SINK}').split(': ')[-1]
        vol_f = float(vol.split()[0])
        muted = vol.endswith(MUTED_SUFFIX)
        return Volume(level=vol_f, muted=muted)

    def toggle_mute(self):
        sh(f'wpctl set-mute {DEFAULT_SINK} toggle')


class Operation:
    INC = 'inc'
    DEC = 'dec'
    TOGGLE = 'toggle'


class Znv:

    def __init__(self):
        self.wpctl = Wpctl()

    def run_operation(self, op: str, step: int | None = None):
        if op == Operation.TOGGLE:
            self.wpctl.toggle_mute()
            return

        if op in [Operation.INC, Operation.DEC]:
            sign = '+' if op == Operation.INC else '-'
            self.wpctl.change_volume(step, sign)
            return

        raise Exception(f'Unknown operation {op}')

    def do_run_operation(self, op: str, step: int | None = None):
        old_volume = self.get_volume()
        self.run_operation(op, step)
        new_volume = self.get_volume()
        if new_volume != old_volume:
            self.notify_volume(new_volume)

    def get_volume(self):
        return self.wpctl.get_volume()

    def get_volume_classifier(self, volume: Volume):
        if volume.muted:
            return 'muted'

        level = volume.level
        if level < 0.3:
            return 'low'
        elif level < 0.6:
            return 'medium'
        elif level <= 1:
            return 'high'
        else:
            return 'overamplified'

    def get_icon(self, volume: Volume):
        classifier = self.get_volume_classifier(volume)
        return VOLUME_ICON.format(icon=classifier)

    def notify_volume(self, volume: Volume):
        icon = self.get_icon(volume)
        volume_i = 0 if volume.muted else volume.level * 100
        sh(f'notify-send -a volume -u low -h int:value:{volume_i} -i {icon} "Volume"')


def do_dec(args: Args, znv: Znv):
    znv.do_run_operation(Operation.DEC, args.step)


def do_inc(args: Args, znv: Znv):
    znv.do_run_operation(Operation.INC, args.step)


def do_toggle(_: Args, znv: Znv):
    znv.do_run_operation(Operation.TOGGLE)


def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    dec = subparsers.add_parser('dec', aliases=['d'])
    dec.add_argument('step', nargs='?', default=DEFAULT_VOLUME_STEP, type=int)
    dec.set_defaults(func=do_dec)

    inc = subparsers.add_parser('inc', aliases=['i'])
    inc.add_argument('step', nargs='?', default=DEFAULT_VOLUME_STEP, type=int)
    inc.set_defaults(func=do_inc)

    toggle = subparsers.add_parser('toggle', aliases=['t'])
    toggle.set_defaults(func=do_toggle)

    pargs = parser.parse_args()
    if not hasattr(pargs, 'func'):
        parser.print_help()
        return

    args = Args(**{k: v for k, v in pargs.__dict__.items() if k != 'func'})

    pargs.func(args, Znv())


if __name__ == '__main__':
    main()

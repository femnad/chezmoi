#!/usr/bin/env python3
import argparse
from dataclasses import dataclass
import eyed3
from eyed3.id3.frames import ImageFrame
import glob
import mimetypes
import os
import rarfile
import requests
import shlex
import shutil
import subprocess
import sys
import tarfile
from typing import Optional
import urllib
import zipfile

COVER_ART_DESCRIPTIONS = [
    '',
    'cover',
    'folder.jpg',
]


@dataclass
class Args:
    skip_confirmation: bool = False
    skip_scan: bool = False
    target: str = ''


@dataclass
class Cover:
    data: bytes
    mime_type: str


def is_within_directory(directory: str, target: str) -> bool:
    abs_directory = os.path.abspath(directory)
    abs_target = os.path.abspath(target)
    prefix = os.path.commonprefix([abs_directory, abs_target])

    return prefix == abs_directory


def is_mac_os_artifact(file: str) -> bool:
    return file.startswith('__MACOSX') or file.endswith('.DS_Store')


def get_parent(files: list[str]) -> str:
    shortest_elm = ''
    for file in files:
        if is_mac_os_artifact(file):
            continue

        if not shortest_elm:
            shortest_elm = file
            continue

        if len(file) < len(shortest_elm):
            shortest_elm = file

    return shortest_elm


def get_confirmation(members: list[str]):
    print('Archive contents:')
    print('\n'.join(members))
    ans = input('Continue? y/N ')
    if not ans.lower().startswith('y'):
        print('Exiting on request')
        sys.exit(1)


def safe_extract_tar(args: Args, tar: tarfile.TarFile) -> list[str]:
    cwd = os.getcwd()
    members = []
    for member in tar.getmembers():
        member_path = os.path.join(cwd, member.name)
        if not is_within_directory(cwd, member_path):
            raise Exception('Attempted path traversal in tar file')

        members.append(member.name)

    if not args.skip_confirmation:
        get_confirmation(members)

    tar.extractall('.')

    return members


def extract_tar(args: Args, filename: str) -> list[str]:
    with tarfile.open(filename) as tf:
        return safe_extract_tar(args, tf)


def safe_extract_zip(args: Args, zf: zipfile.ZipFile) -> list[str]:
    members = zf.namelist()

    if not args.skip_confirmation:
        get_confirmation(members)

    zf.extractall('.')
    return members


def extract_rar(args: Args, filename: str) -> list[str]:
    with rarfile.RarFile(filename) as rf:
        return safe_extract_zip(args, rf)


def extract_zip(args: Args, filename: str) -> list[str]:
    with zipfile.ZipFile(filename) as zf:
        return safe_extract_zip(args, zf)


EXTRACTORS = {
    'application/vnd.rar': extract_rar,
    'application/x-tar': extract_tar,
    'application/zip': extract_zip,
}


def do_extract(args: Args, filename: str) -> list[str]:
    file_types = mimetypes.guess_type(filename)

    if not file_types:
        raise Exception(f'Unable to determine extractor for {filename}')

    file_type = file_types[0]

    if file_type not in EXTRACTORS:
        raise Exception(f'Unable to extract {file_types}')

    fn = EXTRACTORS[file_type]
    return fn(args, filename)


def extract(args: Args, filename: str):
    return do_extract(args, filename)


def fetch_cover() -> Cover:
    url = input('Image URL? ')
    resp = requests.get(url)
    if not resp.ok:
        raise Exception(f'Unexpected response from URL {url}: {resp.status_code}')

    parsed = urllib.parse.urlparse(url)
    mime_type = mimetypes.guess_type(parsed.path)
    assert mime_type[0] is not None
    return Cover(data=resp.content, mime_type=mime_type[0])


def track_has_image(tag: eyed3.id3.tag.Tag):
    for desc in COVER_ART_DESCRIPTIONS:
        if tag.images.get(desc):
            return True

    return False


def sanitize_file(entry: os.DirEntry, num_tracks: int,
                  cover: Optional[Cover]) -> tuple[Optional[eyed3.core.Tag], Optional[Cover]]:
    path = entry.name

    if is_mac_os_artifact(path):
        os.unlink(path)
        return None, None

    if entry.is_dir() and entry.stat().st_mode & 0o777 != 0o755:
        print(f'Set mode to 0755 for {path}')
        os.chmod(path, 0o755)
        return None, None

    if entry.is_file() and entry.stat().st_mode & 0o777 != 0o644:
        print(f'Set mode to 0644 for {path}')
        os.chmod(path, 0o644)

    if not entry.name.endswith('.mp3'):
        return None, None

    should_save = False
    file = eyed3.load(path)
    tag = file.tag

    track_num = tag.track_num
    if not track_num[1]:
        track_num = (track_num[0], num_tracks)

    artist = tag.artist
    if not artist:
        raise Exception('Artist tag is empty')

    if artist == artist.upper():
        artist = artist.title()
        print(f'Updating artist to {artist}')
        tag.artist = artist
        should_save = True

    if tag.album_artist != artist:
        print(f'Updating album artist to {artist}')
        tag.album_artist = artist
        should_save = True

    if not track_has_image(tag):
        print(f'Setting cover art for {entry.name}')
        if not cover:
            cover = fetch_cover()
        tag.images.set(ImageFrame.FRONT_COVER, img_data=cover.data, mime_type=cover.mime_type)
        should_save = True

    if tag.comments:
        comments = tag.comments.get('')
        if comments:
            print(f'`Removing comment {comments.text}`')
            tag.comments.set('')
            should_save = True

    if should_save:
        print('Updating tag')
        tag.save()

    want_name = f'{artist} {tag.album} - {track_num[0]:02} - {tag.title}'
    want_name = want_name.replace('/', '_')
    name_wo_ext = os.path.join(*file.path.split('.')[:-1])
    if want_name != name_wo_ext:
        print(f'Rename file `{name_wo_ext}` to `{want_name}`')
        file.rename(want_name)

    return tag, cover


def sanitize(dirname: str):
    if os.stat(dirname).st_mode & 0o777 != 0o755:
        os.chmod(dirname, 0o755)

    start_dir = os.getcwd()
    os.chdir(dirname)

    tag = None
    num_tracks = len(glob.glob(f'{dirname}{os.sep}*.mp3'))
    cover = None
    with os.scandir('.') as it:
        for entry in it:
            maybe_tag, cover = sanitize_file(entry, num_tracks, cover)
            if not tag and maybe_tag:
                tag = maybe_tag

    os.chdir(start_dir)

    if not tag:
        print('Skipping dir rename as tag is empty')
        return

    assert tag is not None

    want_dirname = f'{tag.artist} - {tag.getBestDate().year} - {tag.album}'
    if want_dirname != dirname:
        print(f'Rename dir `{dirname}` to `{want_dirname}`')
        os.rename(dirname, want_dirname)


def scan():
    subprocess.run(shlex.split('clamscan -r --no-summary'), check=True)


def process(args: Args):
    albums = []
    with os.scandir(args.target) as it:
        for entry in it:
            if entry.is_dir():
                continue

            members = extract(args, entry.name)
            album = get_parent(members)
            if not os.path.isdir(album):
                album = entry.name.rsplit('.', 1)[0]
                os.makedirs(album)
                for m in members:
                    shutil.move(m, album)
            albums.append(album)

    if not args.skip_scan:
        scan()

    for album in albums:
        sanitize(album)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--skip-confirmation', action='store_true')
    parser.add_argument('-s', '--skip-scan', action='store_true')
    parser.add_argument('target', nargs='?', default='.')
    parser.add_argument

    args = Args(**parser.parse_args().__dict__)

    process(args)


if __name__ == '__main__':
    main()

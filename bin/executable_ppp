#!/usr/bin/env python3
from enum import Enum
from dataclasses import dataclass
import shlex
import subprocess
import sys

ICON = 'media-playback-{icon}-symbolic'


@dataclass
class Track:
    album: str
    artist: str
    title: str
    artUrl: str = ''
    albumArtist: str = ''
    autoRating: str = ''
    discNumber: str = ''
    length: str = ''
    trackid: str = ''
    trackNumber: str = ''
    url: str = ''


class State(Enum):
    Paused = 'Paused'
    Playing = 'Playing'


def sh(cmd_str: str):
    cmd = shlex.split(cmd_str)
    return subprocess.run(cmd, capture_output=True, text=True).stdout.strip()


def playerctl(args):
    players = sh('playerctl -l').split('\n')
    maybe_player = ' -p spotify' if 'spotify' in players else ''
    return sh(f'playerctl{maybe_player} {args}')


def track_string(track: Track) -> str:
    return f'{track.title} by {track.artist} from {track.album}'


def notify(state: State, track):
    icon = 'start' if state == State.Playing else 'pause'
    icon = ICON.format(icon=icon)
    sh(f'notify-send -a ppp -i {icon} {state.name} "{track_string(track)}"')


def get_track() -> Track:
    metadata = playerctl('metadata')

    if not metadata:
        sys.exit(1)

    metadata = {
        f[1].split(':')[-1]: f[-1]
        for f in [line.split(maxsplit=2) for line in playerctl('metadata').split('\n')]
    }
    return Track(**metadata)


def play_pause():
    playerctl('play-pause')


def play_or_pause():
    state = playerctl('status')
    if not state:
        return

    if state == 'Playing':
        track = get_track()
        play_pause()
        notify(State.Paused, track)
        return

    play_pause()
    track = get_track()
    notify(State.Playing, track)


def main():
    play_or_pause()


if __name__ == '__main__':
    main()

#!/usr/bin/env python3
import argparse
from dataclasses import dataclass
import re
import shlex
import subprocess
import sys

DEFAULT_REMOTE_NAME = 'origin'
DEFAULT_REMOTE_BRANCH = 'main'
MODIFIED_LINE_REGEX = re.compile(r'^(A|M)\s+(.*)')
UNTRACKED_LINE_REGEX = re.compile(r'^\?\?\s+(.*)')


@dataclass
class Args:
    origin_main: str = ''
    ref: str = ''


def sh(cmdstr: str):
    cmd = shlex.split(cmdstr)
    proc = subprocess.run(cmd, text=True, capture_output=True)

    if proc.returncode:
        output = {}
        if proc.stdout:
            output['stdout'] = proc.stdout.strip()
        if proc.stderr:
            output['stderr'] = proc.stderr.strip()
        msg = ','.join([f'`{cmdstr}` exited with {proc.returncode} {k}: {v}' for k, v in output.items()])
        print(msg)
        sys.exit(proc.returncode)

    return proc.stdout.strip()


def get_remote():
    cmd = shlex.split("git rev-parse --abbrev-ref --symbolic-full-name @'{u}'")
    proc = subprocess.run(cmd, text=True, capture_output=True)

    if proc.returncode:
        stderr = proc.stderr.strip()
        if stderr.startswith('fatal: no upstream configured for branch'):
            return DEFAULT_REMOTE_NAME
        print(stderr)
        sys.exit(1)

    return proc.stdout.strip().split('/')[0]


def list_files_of_interest(args: Args):
    ref = 'origin/main' if args.origin_main else args.ref
    cmd = 'git diff --name-only'
    if ref:
        cmd += f' {ref}'
    else:
        status = sh('git status -s')
        if not status:
            remote = get_remote()
            cmd += f' {remote}/{DEFAULT_REMOTE_BRANCH}'

    git_ls = sh(cmd)
    modified = git_ls.split('\n') if git_ls else []
    status = sh('git status -s').split('\n')

    added = []
    untracked = []

    for f in status:
        if match := MODIFIED_LINE_REGEX.match(f):
            added.append(match.group(2))
        elif match := UNTRACKED_LINE_REGEX.match(f):
            untracked.append(match.group(1))

    files_of_interest = set()
    for f in [modified, added, untracked]:
        files_of_interest.update(f)
    out_list = sorted(list(files_of_interest))

    for f in out_list:
        print(f)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--origin-main', action='store_true')
    parser.add_argument('-r', '--ref')
    args = parser.parse_args()

    pargs = Args(**args.__dict__)

    list_files_of_interest(pargs)


if __name__ == '__main__':
    main()

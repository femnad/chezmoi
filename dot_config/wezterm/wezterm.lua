local wezterm = require 'wezterm'
local config = {}
if wezterm.config_builder then
  config = wezterm.config_builder()
end

config.colors = {
  foreground = '#999993',
  background = '#101010',
  ansi = {
    '#333333',
    '#8c4665',
    '#287373',
    '#7c7c99',
    '#395573',
    '#5e468c',
    '#31658c',
    '#899ca1',
  },
  brights = {
    '#3d3d3d',
    '#bf4d80',
    '#53a6a6',
    '#9e9ecb',
    '#477ab3',
    '#7e62b3',
    '#6096bf',
    '#c0c0c0',
  },
  compose_cursor = 'orange',
  cursor_bg = '#999999',
  cursor_fg = '#464646',
  cursor_border = '#646464',
}

config.default_prog = { '/usr/bin/fish' }
config.enable_tab_bar = false
config.font_size = 14
config.window_padding = {
    left = 0,
    right = 0,
    top = 0,
    bottom = 0,
}

config.keys = {
  {
    key = 'c',
    mods = 'CTRL|ALT',
    action = wezterm.action.CopyTo 'ClipboardAndPrimarySelection',
  },
  {
    key = 'v',
    mods = 'CTRL|ALT',
    action = wezterm.action.PasteFrom 'PrimarySelection',
  },
  {
    key = '-',
    mods = 'LEADER',
    action = wezterm.action.SplitVertical { domain = 'CurrentPaneDomain' },
  },
  {
    key = '|',
    mods = 'LEADER|SHIFT',
    action = wezterm.action.SplitHorizontal { domain = 'CurrentPaneDomain' },
  },
  {
    key = 'c',
    mods = 'LEADER',
    action = wezterm.action.ActivateCopyMode,
  },
  {
    key = 'h',
    mods = 'LEADER',
    action = wezterm.action.ActivatePaneDirection 'Left',
  },
 {
    key = 's',
    mods = 'LEADER',
    action = wezterm.action.ActivatePaneDirection 'Right',
  },
  {
    key = 't',
    mods = 'LEADER',
    action = wezterm.action.ActivatePaneDirection 'Down',
  },
  {
    key = 'n',
    mods = 'LEADER',
    action = wezterm.action.ActivatePaneDirection 'Up',
  },
  {
    key = 'o',
    mods = 'LEADER',
    action = wezterm.action{
      QuickSelectArgs={
        action = wezterm.action_callback(function(window, pane)
          local url = window:get_selection_text_for_pane(pane)
          wezterm.open_with(url)
        end),
        alphabet = 'hutenosadigpcrlfymkwjvqzbx',
        patterns = {
          "https?://\\S+"
        },
      }
    }
  },
  {
    key = 'm',
    mods = 'LEADER|CTRL',
    action = wezterm.action.SendKey { key = 'm', mods = 'CTRL' },
  },
  {
    key = 'u',
    mods = 'LEADER',
    action = wezterm.action.QuickSelect,
  },
}
config.leader = { key = 'm', mods = 'CTRL' }

return config

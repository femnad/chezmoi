config.load_autoconfig()

c.bindings.commands = {
    'normal': {
        'b': 'set-cmd-text -s :tab-select ',
        'u': 'scroll-page 0 -0.5',
        'd': 'scroll-page 0 0.5',
        'gt': 'tab-next',
        'gT': 'tab-next',
        'x': 'tab-close',
        'X': 'undo',
        'T': 'set-cmd-text -s :open -t ',
        't': 'hint all tab-bg',
    },
    'insert': {
        '<Ctrl-f>': 'fake-key <Right>',
        '<Ctrl-b>': 'fake-key <Left>',
        '<Ctrl-a>': 'fake-key <Home>',
        '<Ctrl-e>': 'fake-key <End>',
        '<Ctrl-n>': 'fake-key <Down>',
        '<Ctrl-p>': 'fake-key <Up>',
        '<Alt-v>': 'fake-key <PgUp>',
        '<Ctrl-v>': 'fake-key <PgDown>',
        '<Alt-f>': 'fake-key <Ctrl-Right>',
        '<Alt-b>': 'fake-key <Ctrl-Left>',
        '<Ctrl-d>': 'fake-key <Delete>',
        '<Alt-d>': 'fake-key <Ctrl-Delete>',
        '<Alt-Backspace>': 'fake-key <Ctrl-Backspace>',
        '<Ctrl-y>': 'insert-text {primary}',
    }
}

c.completion.open_categories = ['searchengines', 'history', 'quickmarks']
c.content.blocking.enabled = False

c.downloads.location.prompt = False
c.downloads.remove_finished = 5

c.fonts.default_size = '12pt'

c.tabs.last_close = 'blank'
c.tabs.show = 'multiple'

c.url.default_page = 'about:blank'
c.url.searchengines = {
    'DEFAULT': 'https://duckduckgo.com/?q={}',
    'gh': 'https://github.com/search?q={}',
}
c.url.start_pages = ['about:blank']

;;; base -- common initialisations

;;; Commentary:
;;; Making flycheck happy since 2018

;;; Code:

(require 'package)

(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(use-package avy
  :ensure t)
(use-package company
  :ensure t)
(use-package evil
  :ensure t)
(use-package fish-mode
  :ensure t)
(use-package flycheck
  :ensure t
  :pin melpa-stable)
(use-package jedi
  :ensure t)
(use-package hamburg-theme
  :ensure t)
(use-package ivy-yasnippet
  :ensure t)
(use-package markdown-mode
  :ensure t)
(use-package projectile
  :ensure t)
(use-package smex
  :ensure t)
(use-package switch-window
  :ensure t)
(use-package terraform-mode
  :ensure t
  :pin melpa)
(use-package whitespace-cleanup-mode
  :ensure t)

(show-paren-mode 1)
(setq column-number-mode t)

(global-auto-revert-mode 1)
(global-display-line-numbers-mode)
(setq auto-revert-interval 1)
(setq global-auto-revert-non-file-buffers t)
(setq auto-revert-verbose nil)
(yas-global-mode 1)

(put 'dired-find-alternate-file 'disabled nil)

;; package configurations
(require 'dired-x)
(setq dired-omit-files "^\\...+$")
(add-hook 'dired-mode-hook (lambda() (dired-omit-mode 1)))

(require 'ido)
(ido-mode t)
(setq ido-enable-flex-matching t)

(setq switch-window-shortcut-style 'qwerty)
(setq switch-window-qwerty-shortcuts
      '("h" "u" "t" "e" "n" "o" "s" "a" "d" "i"))

(defalias 'yes-or-no-p 'y-or-n-p)

(setq c-default-style "linux")
(setq-default c-basic-offset 4)

(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq indent-line-function 'insert-tab)

(setq vc-follow-symlinks t)

(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)

(setq windmove-wrap-around t)

(setq avy-background t)
(setq avy-highlight-first t)
(setq avy-keys '(?h ?t ?n ?s ?d ?u ?e ?o ?a ?i))
(setq avy-style 'pre)

(define-prefix-command 'luser)
(if (display-graphic-p)
    (global-set-key (kbd "C-.") 'luser)
  (global-set-key (kbd "C-c C-g") 'luser))

(define-key luser (kbd "a") 'helm-projectile-rg)
(define-key luser (kbd "b") 'projectile-replace)
(define-key luser (kbd "C") 'cua-mode)
(define-key luser (kbd "c") 'cua-set-rectangle-mark)
(define-key luser (kbd "d") 'avy-goto-char-2)
(define-key luser (kbd "e") 'replace-string)
(define-key luser (kbd "F") 'ff-find-other-file)
(define-key luser (kbd "f") 'projectile-find-file)
(define-key luser (kbd "G") 'global-hl-line-mode)
(define-key luser (kbd "g") 'hl-line-mode)
(define-key luser (kbd "h") 'windmove-left)
(define-key luser (kbd "i") 'avy-goto-line)
(define-key luser (kbd "l") 'load-file)
(define-key luser (kbd "m") 'avy-goto-char-2)
(define-key luser (kbd "n") 'windmove-up)
(define-key luser (kbd "o") 'occur)
(define-key luser (kbd "p") 'flycheck-list-errors)
(define-key luser (kbd "r") 'revert-buffer)
(define-key luser (kbd "s") 'windmove-right)
(define-key luser (kbd "t") 'windmove-down)
(define-key luser (kbd "u") 'comment-or-uncomment-region)
(define-key luser (kbd "v") 'view-mode)
(define-key luser (kbd "W") 'global-whitespace-mode)
(define-key luser (kbd "w") 'whitespace-mode)
(define-key luser (kbd "x") 'whitespace-cleanup)
(define-key luser (kbd "y") 'helm-show-kill-ring)
(define-key luser (kbd ".") 'flycheck-next-error)
(define-key luser (kbd ",") 'flycheck-previous-error)
(define-key luser (kbd "SPC") 'switch-window)
(define-key luser (kbd "|") 'split-window-right)
(define-key luser (kbd "_") 'split-window-below)

(define-prefix-command 'puser)
(if (display-graphic-p)
    (global-set-key (kbd "C-,") 'puser)
  (global-set-key (kbd "C-c C-d") 'puser))
(define-key puser (kbd "h") 'ivy-yasnippet)
(define-key puser (kbd "p") (lambda() (interactive) (insert "breakpoint()")))
(define-key puser (kbd "c") 'pycscope-find-this-symbol)
(define-key puser (kbd "r") 'pycscope-find-global-definition)

(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

(add-hook 'after-init-hook #'global-flycheck-mode)
(add-hook 'after-init-hook 'global-company-mode)

;; Company mode configurations
(setq company-idle-delay 0.1)
(setq company-minimum-prefix-length 1)
(setq company-dabbrev-downcase nil)

(electric-indent-mode)

(defun copy-file-name-to-clipboard ()
  "Copy the current buffer file name to the clipboard."
  (interactive)
  (let ((filename (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name))))
    (when filename
      (kill-new filename)
      (message "Copied buffer file name '%s' to the clipboard." filename))))

(setq compilation-read-command nil)

(global-whitespace-cleanup-mode)

(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

(load-library "python-hooks")

(defun save-all ()
  "Save all buffers."
  (interactive)
  (save-some-buffers t))

(add-hook 'focus-out-hook 'save-all)

(setq python-shell-interpreter "python3")

;;; (provide 'base)
;;; base.el ends here

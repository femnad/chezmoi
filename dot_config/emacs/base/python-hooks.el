;;; my-python-hooks --- Summary
;;; Commentary:

;;; Code:
(defun electric-indent-ignore-python (char)
  "Ignore electric indentation for python-mode"
  (if (equal major-mode 'python-mode)
      'no-indent
    nil))

;; Enter key executes newline-and-indent
(defun set-newline-and-indent ()
  "Map the return key with `newline-and-indent'"
  (local-set-key (kbd "RET") 'newline-and-indent))

(defun my/python-mode-hook ()
  (progn
    (use-package company-jedi
      :ensure t)
    (add-to-list 'company-backends 'company-jedi)
    (add-hook 'electric-indent-functions 'electric-indent-ignore-python)
    (set-newline-and-indent)))

(add-hook 'python-mode-hook 'my/python-mode-hook)

(provide 'my-python-hooks)
;;; my-python-hooks ends here

(add-to-list 'load-path "~/.config/emacs/base")
(load-library "base")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode nil)
 '(column-number-mode t)
 '(custom-safe-themes
   '("ace21d57cd9a22c96c021acfd2938411e3374626fe8d91afb9bb969b5269ea75" default))
 '(global-display-line-numbers-mode t)
 '(menu-bar-mode nil)
 '(package-selected-packages
   '(ivy-yasnippet yasnippet k8s-mode projectile-ripgrep gnu-elpa-keyring-update rust-mode company-jedi salt-mode yaml-mode whitespace-cleanup-mode use-package switch-window smex projectile markdown-mode magit jinja2-mode hamburg-theme go-mode flycheck fish-mode dockerfile-mode company avy))
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(when (display-graphic-p)
  (load-theme 'hamburg t))

(scroll-bar-mode 0)

(set-frame-font "Monospace-14")

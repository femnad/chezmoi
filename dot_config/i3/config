# i3 config file (v4)
#
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod
workspace_layout tabbed

set $mod Mod4

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:monospace 12

# class                 border  backgr. text    indicator child_border
client.focused          #000000 #676767 #d3d3d3 #2e9ef4   #000000
client.focused_inactive #333333 #5f676a #767676 #484e50   #5f676a
client.unfocused        #333333 #212121 #898989 #292d2e   #222222
client.urgent           #2f343a #900000 #ffffff #900000   #900000
client.placeholder      #000000 #0c0c0c #ffffff #000000   #0c0c0c

client.background       #ffffff

default_border pixel 1
default_floating_border pixel 1
for_window [class="^.*"] border pixel 1
for_window [class="opterm"] floating enable

set $bkl exec --no-startup-id bkl
set $cmd exec --no-startup-id
set $def mode "default"
set $dun exec --no-startup-id dunstctl
set $exit exec --no-startup-id "i3-nagbar -t warning -m 'Exit i3?' -B 'Yes' 'i3-msg exit'"
set $fpp exec --no-startup-id fpp
set $hsp exec --no-startup-id hsp
set $mi3 exec --no-startup-id mi3
set $nor exec --no-startup-id nor volume
set $rca exec --no-startup-id mi3 rca -c Chromium-browser -e chromium-browser
set $rofi exec --no-startup-id rofi
set $ror exec --no-startup-id mi3 ror
set $ysnp exec --no-startup-id ysnp

bindsym XF86AudioLowerVolume $nor dec
bindsym XF86AudioRaiseVolume $nor inc
bindsym Mod1+XF86AudioLowerVolume $nor dec -t source
bindsym Mod1+XF86AudioRaiseVolume $nor inc -t source
bindsym Shift+XF86AudioLowerVolume $nor dec 1
bindsym Shift+XF86AudioRaiseVolume $nor inc 1
bindsym XF86AudioMute $nor toggle
bindsym XF86AudioPlay $cmd ppp
bindsym XF86AudioNext $cmd playerctl next
bindsym XF86AudioPrev $cmd playerctl previous
bindsym XF86MonBrightnessDown $bkl dec 5
bindsym XF86MonBrightnessUp $bkl inc 5
bindsym Shift+XF86MonBrightnessDown $bkl dec
bindsym Shift+XF86MonBrightnessUp $bkl inc

# toggle tiling / floating
bindsym $mod+space floating toggle
# change focus between tiling / floating windows
bindsym $mod+Shift+space focus mode_toggle

set $ws1 "1"
set $ws2 "2"

bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2

bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2

bindsym $mod+h $dun history-pop
bindsym $mod+t $dun close
bindsym $mod+c $dun close-all

mode "main" {
        bindsym a $def
        bindsym b $mi3 banish; $def
        bindsym c $ror alacritty; $def
        bindsym d $mi3 split; $def
        bindsym e $mi3 exchange; $def
        bindsym f fullscreen toggle; $def
        bindsym g mode "chromium"
        bindsym h mode "launch"
        bindsym i $mi3 join; $def
        bindsym j $ror -e jetbrains-rustrover; $def
        bindsym k $ror -e jetbrains-goland; $def
        bindsym l $ror -c dev.zed.Zed zed; $def
        bindsym m $mi3 same; $def
        bindsym n $ror firefox -c org.mozilla.firefox -i Navigator; $def
        bindsym o $ror code; $def
        bindsym p mode "ysnp"
        bindsym q $ror -c com.mitchellh.ghostty ghostty; $def
        bindsym r bar mode toggle; $def
        bindsym s $def
        bindsym t $mi3 other; $def
        bindsym u bar mode dock; mode "navigate"
        bindsym v $ror alavim -c Nvim; $def
        bindsym w $rofi -show window -matching fuzzy -show-icons -font 'monospace 18'; $def
        bindsym x $ror -c org.wezfurlong.wezterm wezterm; $def
        bindsym y $cmd primary-to-clipboard; $def
        bindsym z kill; $def

        bindsym Ctrl+c $cmd alacritty; $def
        bindsym Ctrl+h $cmd clipmenu; $def
        bindsym Ctrl+m $mi3 same -r; $def
        bindsym Ctrl+n $cmd firefox; $def
        bindsym Ctrl+q $exit; $def
        bindsym Ctrl+r reload; $def
        bindsym Ctrl+s restart; $def
        bindsym Ctrl+w $mi3 r; $def

        bindsym Mod1+c $cmd pcl; $def
        bindsym Mod1+d $cmd nor default sink; $def
        bindsym Mod1+g $cmd flameshot gui; $def
        bindsym Mod1+h $ror -c org.wezfurlong.wezterm wezterm; $def
        bindsym Mod1+i $cmd nor default source; $def
        bindsym Mod1+l $cmd lyk; $def
        bindsym Mod1+p $ror 1password -c 1Password -a clipctl-disable-enable; $def
        bindsym Mod1+r $cmd rdsp -v; $def
        bindsym Mod1+s $ror spotify; $def
        bindsym Mod1+z $ror zeal; $def

        bindsym comma focus left; $def
        bindsym period focus right; $def

        bindsym Return $def
        bindsym Escape $def
        bindsym space $def
}

bindsym Ctrl+t mode "main"

mode "chromium" {
        bindsym d $rca mbjafbmjpcimpkkihihoideiofnoalmh; $def
        bindsym g $ror chromium-browser -i chromium-browser; $def
        # Navidrome
        bindsym n $rca gojefmnmngjcniphdepkonmjpafldbph; $def
        bindsym r $rca bndmnggfngpgmmijcogkkgglhalbpomk; $def
        bindsym s $rca ahkhhdeoadojjmjcikbaiaipmojkmchj; $def
        bindsym y $rca agimnkijcaahngcdmfeangaknmldooml; $def

        bindsym Return $def
        bindsym Escape $def
        bindsym space $def
}

mode "launch" {
        bindsym d $rofi -show drun -matching fuzzy -show-icons; $def
        bindsym h $rofi -show run -matching fuzzy; $def
        bindsym n $cmd dmenu_run -fn 'monospace-12' -nb black -nf white -sb white -sf black -p run -b; $def
        bindsym u $cmd 1pqa; $def
        bindsym t $rofi -show run -disable-history; $def
        bindsym z $cmd lmm; $def

        bindsym Return $def
        bindsym Escape $def
        bindsym space $def
}

mode "navigate" {
        bindsym period move up
        bindsym e move down
        bindsym o move left
        bindsym u move right

        bindsym c focus up
        bindsym t focus down
        bindsym h focus left
        bindsym n focus right

        bindsym i layout stacking
        bindsym d layout tabbed
        bindsym s layout toggle split

        bindsym Return bar mode hide; $def
        bindsym Escape bar mode hide; $def
        bindsym space bar mode hide; $def
}

mode "ysnp" {
        bindsym c $ysnp copy; $def
        bindsym e $ysnp login-tab-pass-enter; $def
        bindsym f $ysnp type-field; $def
        bindsym h $ysnp type-enter; $def
        bindsym n $hsp; $def
        bindsym o $hsp -e; $def
        bindsym p $fpp; $def
        bindsym r $fpp -s; $def
        bindsym s $ysnp login-tab-pass; $def
        bindsym t $ysnp type; $def
        bindsym u $ysnp pass-tab-pass; $def
        bindsym y $ysnp login; $def
        bindsym z $cmd clipboard-clean; $def

        bindsym Return $def
        bindsym Escape $def
        bindsym space $def
}

bar {
        colors {
            binding_mode #000000 #00008b #ffffff
        }
        font pango:monospace 18
        mode hide
        padding 0 0 32px
        position top
        status_command i3status
        tray_output primary
        workspace_buttons no
}

exec_always --no-startup-id wminit

local lualine = require('lualine')

local by_bg_color = '#b22222'

local config = {
    options = {
        icons_enabled = false,
        component_separators = '',
        section_separators = '',
    },
    sections = {
        lualine_a = {'mode'},
        lualine_b = {{
            '%r',
            colored = true,
            color = { bg = by_bg_color },
            cond = function() return vim.bo.readonly end
        }},
        lualine_c = {'%F', '%m'},
        lualine_x = {'filetype'},
        lualine_y = {{
            '"PASTE"',
            color = { bg = by_bg_color },
            cond = function() return vim.o.paste end,
        }},
        lualine_z = {'%l/%L:%v'},
    },
}

lualine.setup(config)

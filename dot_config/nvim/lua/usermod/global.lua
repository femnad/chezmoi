g = vim.g

g.mapleader = ' '

g.EasyMotion_do_mapping = 0
g.EasyMotion_smartcase = 1

g.vim_markdown_folding_disabled = 1
g.vim_markdown_no_default_key_mappings = 1

g.indentLine_fileType = {'yaml'}

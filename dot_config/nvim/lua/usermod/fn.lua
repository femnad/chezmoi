local pickers = require("telescope.pickers")
local finders = require("telescope.finders")
local conf = require("telescope.config").values
local builtin = require("telescope.builtin")

local home = os.getenv("HOME")

require('telescope').setup{
  defaults = {
    mappings = {
      i = {
        ['<C-u>'] = false,
      }
    }
  }
}

chezmoi_managed = function(opts)
  opts = opts or {}
  opts.entry_maker = function(entry)
    return {
      value = home .. '/' .. entry,
      display = entry,
      ordinal = entry,
    }
  end
  pickers
    .new(opts, {
      prompt_title = "Chezmoi Managed Files",
      finder = finders.new_oneshot_job({ "barn", "choose", "chezmoi-uc" }, opts),
      sorter = conf.file_sorter(opts),
      previewer = conf.file_previewer(opts),
    })
    :find()
end

-- Top-level conscious live grep
git_grep = function(opts)
    opts = opts or {}
    local top_level = vim.fn.system('git rev-parse --show-toplevel')
    opts.cwd = string.gsub(top_level, '%s+', '')
    return builtin.live_grep(opts)
end

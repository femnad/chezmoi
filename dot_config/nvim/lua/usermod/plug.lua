local Plug = vim.fn['plug#']

plugins = {
    {'neovim/nvim-lspconfig', '2b802ab'},

    {'easymotion/vim-easymotion', 'b3cfab2'},
    {'junegunn/fzf', 'd01ae55'},
    {'junegunn/fzf.vim', '9ceac71'},

    {'dcampos/cmp-snippy', '9af1635'},
    {'dcampos/nvim-snippy', 'd732f34'},
    {'hrsh7th/cmp-buffer', '3022dbc'},
    {'hrsh7th/cmp-cmdline', '8bc9c4a'},
    {'hrsh7th/cmp-nvim-lsp', '78924d1'},
    {'hrsh7th/cmp-path', '91ff86c'},
    {'hrsh7th/nvim-cmp', 'aee4011'},

    {'NoahTheDuke/vim-just', '838c909'},
    {'dag/vim-fish'},
    {'ericbn/vim-relativize', 'd36a108'},
    {'godlygeek/tabular'},
    {'hashivim/vim-terraform', 'f0b17ac'},
    {'ntpeters/vim-better-whitespace', '1b22dc5'},
    {'preservim/vim-markdown', '7231fa4'},
    {'ryvnf/readline.vim'},
    {'sbdchd/neoformat', '1f79f6e'},
    {'simrat39/rust-tools.nvim', '99fd123'},

    {'nvim-lualine/lualine.nvim', 'bfa0d99'},
    {'tyrannicaltoucan/vim-deep-space', '126d52f'},
    {'fatih/vim-go', '8d76779'},

    {'nvim-lua/plenary.nvim', '4b7e520'},
    {'nvim-telescope/telescope.nvim', '0.1.5'},

    {'isobit/vim-caddyfile', '24fe072'},
    {'kaarmu/typst.vim', 'e72561f'},
    {'lukas-reineke/indent-blankline.nvim', '4541d69'},
    {'numToStr/Comment.nvim', '176e85e'},

    {'hashicorp/terraform-ls', '3f376c3'},
}

tagged_plugins = {
}

vim.call('plug#begin', '~/.local/share/plugged')

for _, pc in pairs(plugins) do
    plugin, commit = pc[1], pc[2]
    if commit then
        Plug(plugin, {commit=commit})
    else
        Plug(plugin)
    end
end

for _, pc in pairs(tagged_plugins) do
    Plug(pc[1], pc[2])
end

vim.call('plug#end')

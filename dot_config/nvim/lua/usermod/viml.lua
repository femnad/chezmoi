vim.cmd 'source ~/.config/nvim/viml/auto.vim'
vim.cmd 'source ~/.config/nvim/viml/bind.vim'
vim.cmd 'source ~/.config/nvim/viml/easymotion.vim'
vim.cmd 'source ~/.config/nvim/viml/func.vim'

vim.cmd 'colorscheme deep-space'

vim.cmd 'source ~/.config/nvim/viml/color.vim'

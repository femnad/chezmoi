local map = vim.api.nvim_set_keymap

local normal_map = {
    ['a'] = {cmd='RG ', no_cr=true},
    ['b'] = {cmd='Buffers'},
    ['c'] = {cmd='GFiles?'},
    ['d'] = {cmd='vim.diagnostic.goto_prev', lua=true},
    ['e'] = {cmd='e'},
    ['E'] = {cmd='e!'},
    ['f'] = {cmd='Neoformat'},
    ['g'] = {cmd='GFiles'},
    ['h'] = {cmd='vim.diagnostic.goto_next', lua=true},
    ['i'] = {cmd='nohl'},
    ['j'] = {cmd='put =strftime(\'%F %T\')'},
    ['k'] = {cmd='TableFormat'},
    ['l'] = {cmd='Git blame'},
    ['n'] = {cmd='set relativenumber!'},
    ['o'] = {cmd='call ToROMode()'},
    ['p'] = {cmd='set paste!'},
    ['q'] = {cmd='set spell'},
    ['r'] = {cmd='call ToRWMode()'},
    ['s'] = {cmd='set list!'},
    -- t => easymotion-prefix
    -- u => u<x> binds
    ['v'] = {cmd='GBrowse'},
    ['x'] = {cmd='x'},
    ['w'] = {cmd='StripWhitespace'},
    ['W'] = {cmd='w'},
    ['y'] = {cmd='Files'},
    ['z'] = {cmd='wq'},
    -- Multi letter
    ['gn'] = {cmd='vim.lsp.buf.rename', lua=true, non_leader=true},
    ['gr'] = {cmd='vim.lsp.buf.references', lua=true, non_leader=true},
    ['uc'] = {cmd='chezmoi_managed', lua=true},
    ['ug'] = {cmd='git_grep', lua=true},
    ['uh'] = {cmd='b#'},
    ['un'] = {cmd='bn'},
    ['ut'] = {cmd='bp'},
}

for key, op in pairs(normal_map) do
    local prefix = ''
    if not op.non_leader then
        prefix = '<Leader>'
    end

    local cmd = ''
    if op.lua then
        cmd = '<cmd>lua '
    end

    cmd = cmd .. op.cmd

    if op.lua then
        cmd = cmd .. '()'
    else
        cmd = ':' .. cmd
    end

    no_cr = op.no_cr
    if not no_cr then
        cmd = cmd .. '<CR>'
    end

    map('n', prefix .. key, cmd, {})
end

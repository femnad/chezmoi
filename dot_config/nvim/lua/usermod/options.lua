o = vim.opt

o.autoread = true
o.autowrite = true
o.clipboard = 'unnamedplus'
o.expandtab = true
o.hlsearch = true
o.ignorecase = true
o.incsearch = true
o.number = true
o.relativenumber = true
o.shiftwidth = 4
o.tabstop = 4
o.termguicolors = true

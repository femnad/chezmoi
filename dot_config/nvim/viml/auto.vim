function AutoSave()
    if strlen(expand("%@")) == 0
        return
    end
    if strlen(&buftype) > 0
        return
    end
    if &readonly
        return
    end

    if &modified
        write
    end
endfunction

autocmd BufEnter * silent! lcd %:p:h
autocmd BufLeave,CursorMoved,InsertLeave * call AutoSave()
autocmd BufNewFile,BufRead *.bu set filetype=config
autocmd BufNewFile,BufRead *.conf set filetype=config
autocmd BufNewFile,BufRead *.md set filetype=markdown textwidth=120
autocmd BufNewFile,BufRead *.yaml set textwidth=120
autocmd BufNewFile,BufRead *.yaml.tpl set filetype=yaml
autocmd BufNewFile,BufRead *.yaml.gotmpl set filetype=yaml
autocmd BufWrite *.go call Neoformat()
autocmd BufNewFile,BufRead /dev/shm/pass.* setlocal noswapfile nobackup noundofile shada=""
autocmd FileType gitcommit set textwidth=100

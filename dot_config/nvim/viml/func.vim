function ToRWMode()
    execute "set noro"
    execute "map d d"
    execute "map q q"
    execute "map u u"
endfunction

function ToROMode()
    execute "set ro"
    execute "map d <C-d>"
    execute "map q :q!<CR>"
    execute "map u <C-b>"
endfunction

function! RipgrepFzf(query, fullscreen)
  let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case -- %s || true'
  let initial_command = printf(command_fmt, shellescape(a:query))
  let reload_command = printf(command_fmt, '{q}')
  let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
  call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
endfunction

command! -nargs=* -bang RG call RipgrepFzf(<q-args>, <bang>0)

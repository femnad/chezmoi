function StatuslinePaste()
    return &paste ? 'PASTE' : ''
endfunction

hi User1 ctermfg=white ctermbg=black
hi User2 ctermfg=green ctermbg=black
hi User3 ctermfg=magenta ctermbg=black
hi User4 ctermfg=red ctermbg=black
hi User5 ctermfg=blue ctermbg=black
hi User6 ctermfg=cyan ctermbg=black
hi User7 ctermfg=yellow ctermbg=black
hi User8 ctermfg=red ctermbg=white
set statusline=
set statusline +=%8*\%r\%*              "readonly status
set statusline +=%{StatuslinePaste()}%*
set statusline +=%1*\ %n\ %*            "buffer number
set statusline +=%3*%y%*                "file type
set statusline +=%4*\ %<%F%*            "full path
set statusline +=%7*%m%*                "modified flag
set statusline +=%2*%=%5l%*             "current line
set statusline +=%2*/%L%*               "total lines
set statusline +=%6*%4v\ %*             "virtual column number
set laststatus=2

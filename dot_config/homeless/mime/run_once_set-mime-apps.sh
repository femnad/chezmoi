#!/usr/bin/env bash
set -euEo pipefail

if ! which xdg-mime > /dev/null 2>&1
then
    exit 0
fi

xdg-mime default org.pwmt.zathura.desktop application/pdf

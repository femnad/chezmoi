function reverse_history_search
  history | fzf --no-sort | read -l command
  if test $command
    commandline -rb $command
  end
end

function fish_hybrid_key_bindings
    for mode in default insert visual
        fish_default_key_bindings -M $mode
        bind -M $mode \cr fuzzy-history
        bind -M $mode \eh fg
        bind -M $mode \el downcase-word
        bind -M $mode \em __fish_man_page
        bind -M $mode \et merge-history
    end
    fish_vi_key_bindings --no-erase
end

function fish_prompt
    set -l _status $status
    color-message -f 8787af (whoami)
    echo -n '@'
    color-message -f 585858 (prompt_hostname)
    echo -n ' '
    set -l _prompt_pwd (prompt_pwd)
    if test (echo -n "$_prompt_pwd" | grep -Eo '/|~' | wc -l) -gt 1
        color-message -f af5787 (dirname $_prompt_pwd)/
        color-message -f 87af00 (basename $_prompt_pwd)
    else
        color-message -f 87af00 $_prompt_pwd
    end
    if _has_jobs
        echo -n ' '
        jobs-prompt
    end
    if _has_last_fzf
        echo -n ' '
        color-message -f cyan '|'(last-fzf-prompt)'|'
    end
    if _has_copy_target
        color-message -f cyan (copy-target-prompt)
    end
    set _second_line false
    if _is_in_git
        echo
        set _second_line true
        color-message -f magenta (git-repo-prompt)
        fish_git_prompt
    end
    echo
    color-message -n -f 5f5f87 (date +'%F %T')
    last-status $_status
    color-message -b 585858 -f 000000 '>'
    echo -n ' '
end

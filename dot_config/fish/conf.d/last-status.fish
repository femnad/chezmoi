function last-status
    set -l last_status $argv[1]
    if test $last_status -ne 0
        color-message -f red " $last_status "
    end
end

function last-fzf-prompt
    shorten-path (echo $lof | awk '{print $1}' | tr -d '\n')
end

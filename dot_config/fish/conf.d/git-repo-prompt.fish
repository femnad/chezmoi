function git-repo-prompt
    git rev-parse >/dev/null 2>&1
    or return
    git rev-parse @'{0}' >/dev/null 2>&1
    or return
    set -l upstream (git rev-parse @'{u}' > /dev/null 2>&1 )
    or return
    set -l remote (echo (git rev-parse --abbrev-ref --symbolic-full-name @'{u}') | awk -F '/' '{print $1}')
    set -l remote_url (git remote get-url "$remote")
    if string match -q -r '[a-zA-Z0-9-]+@[a-zA-Z0-9.-]+:[a-zA-Z0-9._-]+/[a-zA-Z0-9._-]+(.git)?' "$remote_url"
        echo "$remote_url" | awk -F ':' '{print $2}' | sed 's/.git//'
    else if string match -q -r 'http(s)?://.*' "$remote_url"
        echo "$remote_url" | cut -d / -f 4- | sed 's/.git//'
    else
        echo "$remote_url"
    end
end

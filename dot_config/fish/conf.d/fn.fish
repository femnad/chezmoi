function _maybe_deactivate_venv
    if test -n "$VIRTUAL_ENV"
        deactivate
    end
end

function _is_in_git
    git rev-parse >/dev/null 2>&1
end

function _has_copy_target
    test -n "$_copy_target"
end

function _has_last_fzf
    test -n "$lof" -a -f (echo "$lof" | awk '{print $1}')
end

function _has_jobs
    jobs >/dev/null
    test $status -eq 0
end

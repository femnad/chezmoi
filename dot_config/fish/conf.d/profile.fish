set -g fish_key_bindings fish_hybrid_key_bindings

set -x ANSIBLE_CONFIG "$HOME/.config/ansible/ansible.cfg"
set -x AWS_CONFIG_FILE "$HOME/.config/aws/config"
set -x AWS_SHARED_CREDENTIALS_FILE "$HOME/.config/aws/credentials"
set -x CDPATH . "$HOME"
set -x EDITOR nvim
set -x FZF_DEFAULT_OPTS '--tiebreak=length,begin --bind=ctrl-j:accept,ctrl-k:kill-line --cycle'
set -x IPYTHONDIR "$HOME/.config/ipython"
set -x MANPAGER most
set -x MOST_INITFILE "$HOME/.config/most/mostrc"
set -x RIPGREP_CONFIG_PATH "$HOME/.config/ripgrep/ripgreprc"
set -x RLWRAP_HOME "$HOME/.cache/rlwrap"
set -x SOPS_AGE_KEY_FILE "$HOME/.local/share/age/sops"
set -x TAG_ALIAS_FILE '/tmp/tag_aliases'
set -x TAG_ALIAS_PREFIX 't'
set -x TAG_CMD_FMT_STRING 'h {{.Filename}} +{{.LineNumber}}'
set -x TAG_SEARCH_PROG rg
set -x TMUX_PLUGIN_MANAGER_PATH "$HOME/.local/share/tpm"
set -x VAGRANT_HOME "$HOME/.local/share/vagrant"
set -x VENV_DIR $HOME/.local/share/venv
set -x VTAG_ALIAS_FILE '/tmp/vtag_aliases'
set -x W3M_DIR ~/.local/share/w3m

function jobs-prompt
    set_color magenta
    echo -n '['
    jobs -lc | tail -n 1 | awk '{print $NF}' | tr -d \n
    echo -n ']'
    set_color normal
end

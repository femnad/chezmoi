function sanitize-file-name
    if test (count $argv) -ne 1
        echo 'usage: sanitize-file-name <filename>'
        return 1
    end
    echo "$argv[1]" | string replace -a ' ' _
end

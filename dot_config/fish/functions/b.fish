function b
    if test (count $argv) -ne 1
        echo 'usage: b <search result number>'
        return 1
    end
    eval (grep "^alias t$argv=" $TAG_ALIAS_FILE | head -n 1 | cut -d = -f 2 | tr -d "'" | sed 's/^h /v /')
end

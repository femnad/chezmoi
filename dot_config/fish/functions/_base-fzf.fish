function _base-fzf
    argparse 'b/base=' m/multi 'p/preview=' -- $argv
    or return 1

    if test -n "$_flag_b"
        set base $_flag_b
    else
        set base ''
    end

    if test -n "$_flag_m"
        set extra -m
    end

    set preview 'bat '$base'{} --paging never --color always -p'
    if test -n "$_flag_p"
        set preview "$_flag_p"
    end

    fzf --ansi --preview "$preview" --preview-window right:50% --bind=alt-t:preview-page-down,alt-n:preview-page-up,alt-h:jump-accept,alt-u:jump '--jump-labels=hutenosadigpc.r,l'"'"'fymkwjvqz;bx0123456789' $extra
end

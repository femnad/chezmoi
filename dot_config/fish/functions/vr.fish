# Defined in /tmp/fish.A9Gtof/vr.fish @ line 2
function vr
	if test (count $argv) -ne 1
        echo 'usage: vr <venv-name>'
        return 1
    end
    _maybe_deactivate_venv
    set venv_name $argv[1]
    rm -rf "$VENV_DIR/$venv_name"
end

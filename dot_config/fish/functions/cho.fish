# Defined in /tmp/fish.6zXPTb/cho.fish @ line 2
function cho
    if test (count $argv) -lt 2
        echo 'usage: cho <overlay> <args...>'
        return 1
    end
    set -l overlay $argv[1]
    chezmoi $argv[2..-1] -S $HOME/.local/share/chezmoi-$overlay
end

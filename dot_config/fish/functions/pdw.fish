function pdw
    if test (count $argv) -lt 1
        echo 'usage: pdw <executable> [args]'
        return 1
    end

    python3 -m pdb (which $argv[1]) $argv[2..]
end

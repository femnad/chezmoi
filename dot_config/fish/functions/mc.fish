function mc
    if test (count $argv) -ne 1
        echo 'usage: mc <dirname>'
        return 1
    end
    mkdir -p $argv
    cd $argv
end

function _barn-files
    argparse 'e/echo' -- $argv
    or return 1

    set in_git false
    if _is_in_git
        set in_git true
    end

    if $in_git
        pushd (git rev-parse --show-toplevel)
    end

    if test -n "$_flag_e"
        _find-maybe-barn -e -g "$in_git"
    else
        _find-maybe-barn -g "$in_git"
    end
    set find_res $status
    if $in_git
        popd
    end

    return $find_res
end


function cu
    if test (count $argv) -eq 1
        cd (chm cd $argv)
        return
    end
    barn choose chm | _dir-fzf -b "$HOME/.local/share/" | read tgt
    or return 1
    eval (barn choose chm $tgt)
end

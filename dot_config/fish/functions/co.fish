# Defined in /tmp/fish.tNWGC1/co.fish @ line 2
function co
    git rev-parse >/dev/null 2>&1
    if test $status -ne 0
        echo 'Not in a git repo'
        return 1
    end

    set -l branch (git branch | grep -v '*' | awk '{print $1}' | fzf)
    and git co $branch
end

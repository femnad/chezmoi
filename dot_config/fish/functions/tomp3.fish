# Defined in /tmp/fish.ZGrTv3/tomp3.fish @ line 1
function tomp3
    if test (count $argv) -ne 1
        echo 'usage: tomp3 <source>'
        return 1
    end
    set source $argv[1]
    set name (string split . $source | head -n 1)
    ffmpeg -i $source -codec:v copy -codec:a libmp3lame -q:a 2 $name.mp3
end

function g
    barn choose repo | fzf --preview="eza --color=always --git --no-filesize --no-permissions --no-user --time-style long-iso -l ~/z/{}" --bind='alt-t:preview-page-down,alt-n:preview-page-up,alt-h:jump-accept,alt-u:jump' | read -l sel
    and eval (barn choose repo $sel)
end

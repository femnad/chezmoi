function _modified-git-files
    set -l exec $argv[1]
    set argv $argv[2..-1]

    set toplevel (git rev-parse --show-toplevel)
    pushd "$toplevel"
    gfof $argv | fzf --bind 'alt-t:preview-page-down,alt-n:preview-page-up,alt-h:jump-accept,alt-u:jump' --preview='git diff origin/main --color=always -- {}' --preview-window 'right:60%' --jump-labels='hutenosadigpc.r,l\'fymkwjvqz;bx0123456789' -m | read -l tgt
    if test -z "$tgt"
        return
    end
    "$exec" "$tgt"
    popd
end

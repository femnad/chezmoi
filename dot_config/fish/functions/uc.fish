function uc
    set chezmoi ''
    if test (count $argv) -eq 1
        set chezmoi $argv[1]
    end

    _barn-chezmoi -b "$HOME/" -c "$chezmoi" uc
end

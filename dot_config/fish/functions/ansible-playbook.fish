function ansible-playbook
    if test -f ansible.cfg
        set -e ANSIBLE_CONFIG
    end

    command ansible-playbook $argv
end

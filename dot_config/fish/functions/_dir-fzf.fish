function _dir-fzf
    argparse 'b/base=' -- $argv
    or return 1

    set base ''
    if test -n "$_flag_b"
        set base "$_flag_b/"
    end

    _base-fzf -p 'eza --group-directories-first -l --color=always --git --no-filesize --no-permissions --no-user --no-time '$base'{}' -- -1 $argv
end

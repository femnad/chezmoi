function utcdate
    date --utc +"%Y-%m-%d-%H-%M-%S%z" $argv
end

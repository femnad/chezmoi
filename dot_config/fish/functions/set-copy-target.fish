function set-copy-target
    argparse -s -N 1 h/help -- $argv
    or return 1
    set -U _copy_target (realpath $argv[1])
end

function fuzzy-history
    set cmd_to_eval (history | fzf --tiebreak begin,index)
    commandline $cmd_to_eval
end

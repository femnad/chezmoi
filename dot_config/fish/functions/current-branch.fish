function current-branch
    echo (git status | head -n 1 | cut -d ' ' -f 3)
end

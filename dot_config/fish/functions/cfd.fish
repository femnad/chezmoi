function cfd
    argparse --min-args=1 'h/help' 's/source=' -- $argv
    or return

    set tgt (chm cd "$_flag_s")
    pushd $tgt
    fd $argv
    popd $tgt
end

# Defined in /tmp/fish.2kIyPL/ccd.fish @ line 2
function ccd
    if test (count $argv) -eq 1
        cd (chezmoi source-path -S $HOME/.local/share/chezmoi-$argv[1])
    else
        cd (chezmoi source-path)
    end
end

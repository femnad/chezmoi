function gu
    if test (count $argv) -ne 0
        echo 'usage: ug'
        return 1
    end
    git rev-parse > /dev/null 2>&1
    if test $status -ne 0
        echo 'Not in a git repository'
        return 1
    end
    cd (git rev-parse --show-toplevel)
end

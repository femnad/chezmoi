function m
    if test (count $argv) -ne 1
        echo 'usage: m <search result number>'
        return 1
    end
    eval (grep "alias t$argv=" $TAG_ALIAS_FILE | cut -d = -f 2 | tr -d "'")
end

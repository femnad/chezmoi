function fi
    pushd (git rev-parse --show-toplevel)
    fd $argv
    popd
end

function o
    fd -t f | fzf | read -l file
    switch (file --brief --mime-type "$file")
        case 'text/*'
            hilo "$file"
        case '*'
            xdg-open "$file" &; disown
    end
end

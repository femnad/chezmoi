function ch
    if test (count $argv) -eq 1
        pushd (chezmoi source-path -S $HOME/.local/share/chezmoi-$argv[1])
    else
        pushd (chezmoi source-path)
    end
end

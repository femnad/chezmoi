function vc
    argparse -N 1 -n vc h -- $argv
    or return 1

    set venv_name $argv[1]

    virtualenv -p (which python3) "$VENV_DIR/$venv_name"

    source "$VENV_DIR/$venv_name/bin/activate.fish"
end

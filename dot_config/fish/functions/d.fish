function d
    set in_git false
    if _is_in_git
        set in_git true
    end

    if $in_git
        pushd (git rev-parse --show-toplevel)
    end

	set -l dir (_find-maybe-barn -e -g "$in_git" -t d -i gitdir -n -p 'eza --group-directories-first -l --color=always --git --no-filesize --no-permissions --no-user --no-time {}')
    if test -z "$dir"
        if $in_git
            popd
        end
        return 1
    end

    cd "$dir"
end

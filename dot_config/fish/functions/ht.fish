function hg
    git rev-parse > /dev/null 2>&1
    if test $status -eq 0
        set toplevel (git rev-parse --show-toplevel)
        command tag --follow --hidden $argv $toplevel
    else
        command tag --follow --hidden $argv
    end
end

function ct
    argparse 'h/help' 's/source=' -- $argv
    or return 1

    if test -n "$_flag_s"
        _barn-chezmoi ct -b $_flag_s $argv
    else
        _barn-chezmoi ct $argv
    end
end

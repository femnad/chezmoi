function y
    fd -HL -t d | _base-fzf -p 'eza --group-directories-first -l --color=always --git --no-filesize --no-permissions --no-user --no-time {}' -- -1 | read -l dir
    or return 1
    cd "$dir"
end

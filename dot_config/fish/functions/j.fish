function j
    barn choose bmk | _base-fzf -p 'fd-preview '$HOME'/{}' | read sel
    if test -z "$sel"
        return 1
    end
    barn choose bmk "$sel"
    set sel "$HOME/$sel"
    if test -d "$sel"
        cd "$sel"
    else
        v "$sel"
    end
end

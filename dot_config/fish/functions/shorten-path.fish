function shorten-path
    if test (count $argv) -eq 2
        set shortened_length $argv[2]
    else
        set shortened_length 1
    end

    set -l path (realpath $argv[1])
    set realhome ~
    set -l tmp (string replace -r '^'"$realhome"'($|/)' '~$1' $path)

    string replace -ar '(\.?[^/]{'"$shortened_length"'})[^/]*/' '$1/' $tmp
end

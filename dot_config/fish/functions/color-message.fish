function color-message
    argparse -s b/bg= f/fg= n/nl -- $argv
    or return 1
    if test (count $argv) -eq 0
        return
    end
    if test -n "$_flag_fg"
        set_color "$_flag_fg"
    end
    if test -n "$_flag_bg"
        set_color -b "$_flag_bg"
    end
    if test -n "$_flag_nl"
        echo "$argv"
    else
        echo -n "$argv"
    end
    set_color normal
    set_color -b normal
end

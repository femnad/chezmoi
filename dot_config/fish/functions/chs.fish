# Defined in /tmp/fish.hEzOdl/chs.fish @ line 1
function chs
    if test (count $argv) -gt 0
        chezmoi git status -S $HOME/.local/share/chezmoi-$argv[1] -- -s
    else
        chezmoi git status -- -s
    end
end

function uh
	_maybe_deactivate_venv

    if test (count $argv) -eq 1
        set vst $argv[1]
    else
        barn choose venv | fzf | read vst
        or return 1
    end

    eval (barn choose venv $vst)
end

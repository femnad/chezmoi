function gls
    set count 5
    if test (count $argv) -gt 0
        set count $argv[1]
    end
    git log --format=format:'%C(auto,magenta)%h%Creset %C(auto,blue)%an%Creset %s %C(auto,green)%ar%Creset' -n $count --reverse
end

function _fd-preview
    argparse -n 1 -- $argv

    set target $argv[1]
    if test -f "$target"
        bat --paging never --color always "$target"
    else if test -d "$target"
        eza --group-directories-first -l --color=always --git --no-filesize --no-permissions --no-user --no-time "$target"
    end
end

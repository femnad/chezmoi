function clr
    for i in (docker image ls --format json | jq .ID -r)
        docker image rm -f $i
    end
end

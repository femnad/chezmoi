function ansible
    if test -f ansible.cfg
        set -e ANSIBLE_CONFIG
    end

    command ansible $argv
end

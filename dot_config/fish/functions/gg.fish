function gg
    pushd (git rev-parse --show-toplevel)
    tg $argv
    popd
end

function path-to-clipboard
    if test (count $argv) -eq 1
        set path $argv[1]
    else
        set path (realpath .)
    end
    echo -n (realpath $path) | xclip -selection clipboard
end

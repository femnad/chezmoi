function rlc
    set cmd (history -n 2 search | tail -n +2)
    if test $cmd = 'rlc'
        return
    end
    color-message -f magenta -n "$cmd"
    eval "$cmd"
end

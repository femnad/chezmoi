function _find-maybe-barn
    argparse 'e/echo' 'g/git=' 'i/id=' 'n/no-set-var' 'p/preview=' 't/type=' -- $argv
    or return 1

    set type f
    if test -n "$_flag_t"
        set type "$_flag_t"
    end

    set id git
    if test -n "$_flag_i"
        set id "$_flag_i"
    end

    set preview ''
    if test -n "$_flag_p"
        set preview "$_flag_p"
    end

    set in_git false
    if test -n "$_flag_g"
        set in_git "$_flag_g"
    else if _is_in_git
        set in_git true
    end

    if $in_git
        barn choose $id | _base-fzf -p "$preview" | read relpath
    else
        fd -HL -t $type | _base-fzf -p "$preview" | read relpath
    end

    if test -n "$relpath"
        if $in_git
            barn choose $id "$relpath"
        end
        set -l target (realpath "$relpath")
        if test -z "$_flag_n"
            set -gx lof "$target"
        end
        if test -n "$_flag_e"
            echo "$target"
        end
        return 0
    end

    return 1
end

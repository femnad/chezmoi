function hw
    if test (count $argv) -ne 1
        echo 'usage: hw <executable>'
        return 1
    end
    h (which $argv[1])
end

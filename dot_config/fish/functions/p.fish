function p
    barn choose z-dirs | fzf --preview="eza -r --sort modified --git --no-filesize --no-permissions --no-user --time-style long-iso -l ~/z/{}" | read -l target
    and eval (barn choose z-dirs $target)
end

function ow
    for a in $PATH
       find $a -type f -executable | awk -F '/' '{print $NF}'
    end | sort -u | fzf | read -l executable
    and fd -t f | fzf --prompt "$executable " | read -l arg
    and eval "$executable $arg"
end

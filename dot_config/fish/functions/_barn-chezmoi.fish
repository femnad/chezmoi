function _barn-chezmoi
    argparse 'b/base=' 'c/chezmoi=' -- $argv
    or return 1

    set -l action $argv[1]

    if test -n "$_flag_b"
        set base "$_flag_b"
    else
        set base ''
    end

    if test -n "$_flag_c"
        set chezmoi "$_flag_c"
    else
        set chezmoi ''
    end

    set chezmoi_dir (chm cd "$chezmoi")
    if test -z "$chezmoi_dir"
        set chezmoi_dir ''
    end

    if test (count $argv) -gt 1
        cfd -s "$chezmoi_dir" -- -t f $argv[2..] | _base-fzf -b $base -- -1 | read selection
        or return 1
        bat --paging never "$chezmoi_dir/$selection"
    else
        barn choose chezmoi-$action -e "$chezmoi" | _base-fzf -b $base | read selection
        or return 1
        eval (barn choose chezmoi-$action $selection)
    end
end

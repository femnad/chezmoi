function vd
    if test (count $argv) -ne 1
        echo 'usage: ve <venv-name>'
        return 1
    end

    rm -r "$VENV_DIR/$argv[1]"
end

# Defined in /tmp/fish.A9Gtof/vr.fish @ line 2
function vu
    if test (count $argv) -eq 0
        vl | fzf | read -l venv
        and vs $venv
        $venv
    else
        vs $argv[1]
        $argv
    end
end

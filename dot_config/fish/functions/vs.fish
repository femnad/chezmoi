function vs
    if test (count $argv) -ne 1
        echo 'usage: vs <venv-name>'
        return 1
    end

    _maybe_deactivate_venv

    source "$VENV_DIR/$argv[1]/bin/activate.fish"
end

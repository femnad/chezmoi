# Defined in /tmp/fish.i4w1Yh/tu.fish @ line 2
function tu
    if pgrep -fx tmux >/dev/null
        tmux a $argv
        return
    end

    tmux
end

# Defined in /tmp/fish.zkKGgF/cmo.fish @ line 1
function cmo
    if test (count $argv) -ne 2
        echo 'usage: cmo <command> <override>'
        return 1
    end
    set -l command $argv[1]
    set -l override $argv[2]
    chezmoi $command -S $HOME/.local/share/chezmoi-$override
end

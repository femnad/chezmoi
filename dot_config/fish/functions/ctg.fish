function ctg
    argparse --min-args=1 'h/help' 's/source=' 't/type=' -- $argv
    or return

    set tgt (chm cd $_flag_s)
    pushd $tgt

    if test -n "$_flag_t"
        tg -t "$_flag_t" $argv
    else
        tg $argv
    end

    popd $tgt
end

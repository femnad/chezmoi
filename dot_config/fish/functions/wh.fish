function wh
    set -l num_args (count $argv)

    if test $num_args -eq 0
        echo 'usage: wotcher [-n <sleep-period>] <command>'
        return 1
    end

    if test $argv[1] = '-n'
        if test $num_args -lt 3
            echo 'usage: wotcher [-n <sleep-period>] <command>'
            return 2
        end
        set sleep_period $argv[2]
        set args $argv[3..$num_args]
    else
        set sleep_period 1
        set args $argv
    end

    while true
        date +'%F %T'
        eval "$args"
        sleep $sleep_period
    end
end

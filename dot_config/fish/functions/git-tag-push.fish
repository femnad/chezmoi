function git-tag-push
    if test (count $argv) -ne 1
        echo 'usage: git-tag-push <tag>'
        return 1
    end
    git tag $argv
    git push
    git push --tags
end

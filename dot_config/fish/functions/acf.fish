function acf
    if test (count $argv) -ne 2
        echo 'usage: asf <fn> <prompt>'
        return 1
    end

    set -l fn $argv[1]
    set -l prompt $argv[2]
    set -l fs (gfof)
    or return 1

    if test -z "$fs"
        return 1
    end

    echo "$fs" | fzf --prompt "$prompt> " --preview 'bat --color always --style changes,numbers {}' | read -l tgt
    and "$fn" "$tgt"
end

function yd
    if test (count $argv) -gt 0
        set tgt $argv[1]
    else
        set tgt (petname 2)
    end
    set tgt "$HOME/y/$tgt"
    mkdir -p $tgt
    cd $tgt
    pwd
end
